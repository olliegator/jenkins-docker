build:
	docker build . -t amtal/jenkins-docker

publish: build
	docker push amtal/jenkins-docker
