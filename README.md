Docker image of Jenkins with docker installed

Build:

docker build . -t amtal/jenkins-docker

Run with:

mkdir /var/jenkins_home
chown -R 1000:docker
chmod 700 /var/jenkins_home

docker run -p 8888:8080 -u 1000:$(getent group docker | cut -d: -f3) -v /var/run/docker.sock:/var/run/docker.sock -v /var/jenkins_home:/var/jenkins_home -e JENKINS_OPTS="--prefix=/jenkins" --name jenkins-docker amtal/jenkins-docker

Documentation:

https://github.com/jenkinsci/docker/blob/master/README.md

Nexus:

Run with:

mkdir /var/nexus-data && chown -R 200 /var/nexus-data && chmod 700 /var/nexus-data
docker run -d -p 8081:8081 --name nexus -v /var/nexus-data:/nexus-data sonatype/nexus3

Documentation:

https://hub.docker.com/r/sonatype/nexus3#running

Run with docker compose:

. jenkins.env && docker-compose up

